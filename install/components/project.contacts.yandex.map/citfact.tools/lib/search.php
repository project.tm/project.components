<?php

/*
 * This file is part of the Studio Fact package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Citfact\Tools;

class Search extends \CSearch
{
    /**
     * @param array $arParams
     * @param array $aSort
     * @param array $aParamsEx
     * @param bool|false $bTagsCloud
     * @return void
     */
    public function Search($arParams, $aSort = array(), $aParamsEx = array(), $bTagsCloud = false)
    {
        $DB = \CDatabase::GetModuleConnection('search');

        if (!is_array($arParams))
            $arParams = array("QUERY" => $arParams);

        if (!is_set($arParams, "SITE_ID") && is_set($arParams, "LID")) {
            $arParams["SITE_ID"] = $arParams["LID"];
            unset($arParams["LID"]);
        }

        if (array_key_exists("TAGS", $arParams)) {
            $this->strTagsText = $arParams["TAGS"];
            $arTags = explode(",", $arParams["TAGS"]);
            foreach ($arTags as $i => $strTag) {
                $strTag = trim($strTag);
                if (strlen($strTag))
                    $arTags[$i] = str_replace("\"", "\\\"", $strTag);
                else
                    unset($arTags[$i]);
            }

            if (count($arTags))
                $arParams["TAGS"] = '"' . implode('","', $arTags) . '"';
            else
                unset($arParams["TAGS"]);
        }

        $this->strQueryText = $strQuery = trim($arParams["QUERY"]);
        $this->strTags = $strTags = $arParams["TAGS"];

        if ((strlen($strQuery) <= 0) && (strlen($strTags) > 0)) {
            $strQuery = $strTags;
            $bTagsSearch = true;
        } else {
            if (strlen($strTags))
                $strQuery .= " " . $strTags;
            $strQuery = preg_replace_callback("/&#(\\d+);/", array($this, "chr"), $strQuery);
            $bTagsSearch = false;
        }

        $result = \CSearchFullText::GetInstance()->search($arParams, $aSort, $aParamsEx, $bTagsCloud);
        if (is_array($result)) {
            $this->error = \CSearchFullText::GetInstance()->getErrorText();
            $this->errorno = \CSearchFullText::GetInstance()->getErrorNumber();
            $this->formatter = \CSearchFullText::GetInstance()->getRowFormatter();
            if ($this->errorno > 0)
                return;
        } else {
            if (!array_key_exists("STEMMING", $aParamsEx))
                $aParamsEx["STEMMING"] = \COption::GetOptionString("search", "use_stemming", "N") == "Y";

            $this->Query = new \CSearchQuery("and", "yes", 0, $arParams["SITE_ID"]);
            if ($this->_opt_NO_WORD_LOGIC)
                $this->Query->no_bool_lang = true;

            $query = $this->Query->GetQueryString((BX_SEARCH_VERSION > 1 ? "sct" : "sc") . ".SEARCHABLE_CONTENT", $strQuery, $bTagsSearch, $aParamsEx["STEMMING"], $this->_opt_ERROR_ON_EMPTY_STEM);
            if (!$query || strlen(trim($query)) <= 0) {
                if ($bTagsCloud) {
                    $query = "1=1";
                } else {
                    $this->error = $this->Query->error;
                    $this->errorno = $this->Query->errorno;
                    return;
                }
            }

            if (strlen($query) > 2000) {
                $this->error = GetMessage("SEARCH_ERROR4");
                $this->errorno = 4;
                return;
            }
        }

        foreach (GetModuleEvents("search", "OnSearch", true) as $arEvent) {
            $r = "";
            if ($bTagsSearch) {
                if (strlen($strTags))
                    $r = ExecuteModuleEventEx($arEvent, array("tags:" . $strTags));
            } else {
                $r = ExecuteModuleEventEx($arEvent, array($strQuery));
            }
            if ($r <> "")
                $this->url_add_params[] = $r;
        }

        if (is_array($result)) {
            $r = new \CDBResult;
            $r->InitFromArray($result);
        } elseif (
            BX_SEARCH_VERSION > 1
            && count($this->Query->m_stemmed_words_id)
            && array_sum($this->Query->m_stemmed_words_id) === 0
        ) {
            $r = new \CDBResult;
            $r->InitFromArray(array());
        } else {
            $this->strSqlWhere = "";
            $bIncSites = false;

            $arSqlWhere = array();
            if (is_array($aParamsEx) && !empty($aParamsEx)) {
                foreach ($aParamsEx as $aParamEx) {
                    $strSqlWhere = \CSearch::__PrepareFilter($aParamEx, $bIncSites);
                    if ($strSqlWhere != "")
                        $arSqlWhere[] = $strSqlWhere;
                }
            }
            if (!empty($arSqlWhere)) {
                $arSqlWhere = array(
                    "\n\t\t\t\t(" . implode(")\n\t\t\t\t\tOR(", $arSqlWhere) . "\n\t\t\t\t)",
                );
            }

            $strSqlWhere = \CSearch::__PrepareFilter($arParams, $bIncSites);
            if ($strSqlWhere != "")
                array_unshift($arSqlWhere, $strSqlWhere);

            $strSqlOrder = $this->__PrepareSort($aSort, "sc.", $bTagsCloud);

            if (!array_key_exists("USE_TF_FILTER", $aParamsEx))
                $aParamsEx["USE_TF_FILTER"] = \COption::GetOptionString("search", "use_tf_cache") == "Y";

            $bStem = !$bTagsSearch && count($this->Query->m_stemmed_words) > 0;
            //calculate freq of the word on the whole site_id
            if ($bStem && count($this->Query->m_stemmed_words)) {
                $arStat = $this->GetFreqStatistics($this->Query->m_lang, $this->Query->m_stemmed_words, $arParams["SITE_ID"]);
                $this->tf_hwm_site_id = (strlen($arParams["SITE_ID"]) > 0 ? $arParams["SITE_ID"] : "");

                //we'll make filter by it's contrast
                if (!$bTagsCloud && $aParamsEx["USE_TF_FILTER"]) {
                    $hwm = false;
                    foreach ($this->Query->m_stemmed_words as $i => $stem) {
                        if (!array_key_exists($stem, $arStat)) {
                            $hwm = 0;
                            break;
                        } elseif ($hwm === false) {
                            $hwm = $arStat[$stem]["TF"];
                        } elseif ($hwm > $arStat[$stem]["TF"]) {
                            $hwm = $arStat[$stem]["TF"];
                        }
                    }

                    if ($hwm > 0) {
                        $arSqlWhere[] = "st.TF >= " . number_format($hwm, 2, ".", "");
                        $this->tf_hwm = $hwm;
                    }
                }
            }

            if (!empty($arSqlWhere)) {
                $this->strSqlWhere = "\n\t\t\t\tAND (\n\t\t\t\t\t(" . implode(")\n\t\t\t\t\tAND(", $arSqlWhere) . ")\n\t\t\t\t)";
            }

            if ($bTagsCloud)
                $strSql = $this->tagsMakeSQL($query, $this->strSqlWhere, $strSqlOrder, $bIncSites, $bStem, $aParamsEx["LIMIT"]);
            else
                $strSql = $this->MakeSQL($query, $this->strSqlWhere, $strSqlOrder, $bIncSites, $bStem);

            $strSql = $this->replaceSelect($strSql);
            $r = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
        }

        parent::CDBResult($r);
    }

    /**
     * @param string $sql
     * @return string
     */
    private function replaceSelect($sql)
    {
        $select = array(
            'sc.ID',
            'sc.MODULE_ID',
            'sc.ITEM_ID',
            'sc.PARAM1',
            'sc.PARAM2',
        );
        
        if (strpos($sql, '1c_catalog') !== false) {
            // Query: ЛСР (RAUF Fassade) "Красный" кора М150 250*120*65 мм
            if (strpos($sql, 'sct.SEARCHABLE_CONTENT') !== false) {
                array_push($select, 'sct.SEARCHABLE_CONTENT');
            }

            $sql = preg_replace(
                '/(SELECT\s?(DISTINCT)?\s?).*(,\s?.*RANK\s?FROM.*)/',
                sprintf('$1%s$3', implode(', ', $select)),
                trim(preg_replace('/\s+/', ' ', $sql))
            );
        }

        return $sql;
    }
}