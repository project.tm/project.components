<?
/*
 * This file is part of the Studio Fact package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Citfact\Tools\Catalog\Services;


trait CompareService {

    protected static $defaultType = "CATALOG_COMPARE_LIST";

    protected static $defaultIblock = IBLOCK_CATALOG;

    public static function addToCompare($elementId = false, $type = false, $iblock = false) {
        if(!$elementId)
            return false;

        $type = $type ?: self::$defaultType;
        $iblock = $iblock ?: self::$defaultIblock;

        $result = array(
            "success" => true,
            "errors" => array()
        );

        $arSelect = array(
            "ID",
            "IBLOCK_ID",
            "IBLOCK_SECTION_ID",
            "NAME",
            "DETAIL_PAGE_URL",
        );

        if (!isset($_SESSION[$type][$iblock]["ITEMS"]))
            $_SESSION[$type][$iblock]["ITEMS"] = array();

        try {
            if(!$elementId)
                throw new \LogicException("Укажите ID элемента");
            if(isset($_SESSION[$type][$iblock]["ITEMS"]["ITEMS"][$elementId]))
                throw new Exception("Элемент уже в сравнении");

            $arElement = self::getComparedElement($elementId, $iblock, $arSelect);
            if(empty($arElement["FIELDS"]))
                throw new \LogicException("Не существует элемента");

            if($arElement["IS_OFFER"]) {
                $arElement["FIELDS"] = self::getComparedOfferElement($arElement, $arSelect);
            }

            self::addToCompareSession($arElement, $elementId, $type, $iblock);

        } catch(\LogicException $exception) {
            $result["success"] = false;
            $result["errors"][] = $exception->getMessage();
        }

        return $result;
    }

    public static function removeFromCompare($elementId = false, $type = false, $iblock = false) {
        if(!$elementId)
            return false;

        $type = $type ?: self::$defaultType;
        $iblock = $iblock ?: self::$defaultIblock;

        unset($_SESSION[$type][$iblock]['ITEMS'][$elementId]);
    }

    public function inCompare($elementId = false, $type = false, $iblock = false) {
        if(!$elementId)
            return false;

        $type = $type ?: self::$defaultType;
        $iblock = $iblock ?: self::$defaultIblock;

        return isset($_SESSION[$type][$iblock]['ITEMS'][$elementId]);
    }

    protected function getComparedOfferElement($arElement, $arSelect) {
        $rsPropertyFilter = array(
            "ID" => $arElement["OFFERS"]["OFFERS_PROPERTY_ID"],
            "EMPTY" => "N"
        );
        $rsMasterProperty = \CIBlockElement::GetProperty(
            $arElement["FIELDS"]["IBLOCK_ID"],
            $arElement["FIELDS"]["ID"],
            array(),
            $rsPropertyFilter
        );
        $arMasterProperty = $rsMasterProperty->Fetch();

        if(!$arMasterProperty['VALUE'])
            throw new \LogicException("У предложения нет привязки к товару");

        $rsElementFilter = array(
            'ID' => $arMasterProperty['VALUE'],
            'IBLOCK_ID' => $arMasterProperty['LINK_IBLOCK_ID'],
            'ACTIVE' => 'Y',
        );

        $rsMaster = \CIBlockElement::GetList(array(), $rsElementFilter, false, false, $arSelect);
        $arMaster = $rsMaster->GetNext();

        if(!$arMaster)
            throw new \LogicException("У предложения неверная привязка к товару");

        $arMaster['NAME'] = $arElement["FIELDS"]['NAME'];
        return $arMaster;

    }

    protected function getComparedElement($elementId, $iblock, $arSelect) {
        $arOffers = \CIBlockPriceTools::GetOffersIBlock($iblock);
        $offersIblock = $arOffers ? $arOffers["OFFERS_IBLOCK_ID"]: 0;

        $arFilter = array(
            "ID" => $elementId,
            "IBLOCK_LID" => SITE_ID,
            "IBLOCK_ACTIVE" => "Y",
            "ACTIVE_DATE" => "Y",
            "ACTIVE" => "Y",
            "CHECK_PERMISSIONS" => "Y",
            "MIN_PERMISSION" => "R"
        );

        $arFilter["IBLOCK_ID"] = ($offersIblock > 0 ? array($iblock, $offersIblock) : $iblock);

        $rsElement = \CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        $arElement = $rsElement->GetNext();

        return array(
            "FIELDS" => $arElement,
            "IS_OFFER" => $arElement["IBLOCK_ID"] == $offersIblock,
            "OFFERS" => $arOffers,
            "OFFERS_IBLOCK" => $offersIblock
        );
    }

    protected function addToCompareSession($arElement, $elementId, $type, $iblock) {
        $sectionsList = array();
        $sectionsIterator = \Bitrix\Iblock\SectionElementTable::getList(array(
            'select' => array('IBLOCK_SECTION_ID'),
            'filter' => array('=IBLOCK_ELEMENT_ID' => $arElement["FIELDS"]['ID'], '=ADDITIONAL_PROPERTY_ID' => null)
        ));

        while ($section = $sectionsIterator->fetch()) {
            $sectionId = (int)$section['IBLOCK_SECTION_ID'];
            $sectionsList[$sectionId] = $sectionId;
        }

        $_SESSION[$type][$iblock]['ITEMS'][$elementId] = array(
            'ID' => $arElement["FIELDS"]['ID'],
            '~ID' => $arElement["FIELDS"]['~ID'],
            'IBLOCK_ID' => $arElement["FIELDS"]['IBLOCK_ID'],
            '~IBLOCK_ID' => $arElement["FIELDS"]['~IBLOCK_ID'],
            'IBLOCK_SECTION_ID' => $arElement["FIELDS"]['IBLOCK_SECTION_ID'],
            '~IBLOCK_SECTION_ID' => $arElement["FIELDS"]['~IBLOCK_SECTION_ID'],
            'NAME' => $arElement["FIELDS"]['NAME'],
            '~NAME' => $arElement["FIELDS"]['~NAME'],
            'DETAIL_PAGE_URL' => $arElement["FIELDS"]['DETAIL_PAGE_URL'],
            '~DETAIL_PAGE_URL' => $arElement["FIELDS"]['~DETAIL_PAGE_URL'],
            'SECTIONS_LIST' => $sectionsList,
            'PARENT_ID' => $elementId,
            /*'DELETE_URL' => htmlspecialcharsbx($GLOBALS["APPLICATION"]->GetCurPageParam(
                $arParams['ACTION_VARIABLE']."=DELETE_FROM_COMPARE_RESULT&".$arParams['PRODUCT_ID_VARIABLE']."=".$elementId,
                array($arParams['ACTION_VARIABLE'], $arParams['PRODUCT_ID_VARIABLE'])
            ))*/
        );
    }

    public static function clearCompareList($type, $iblock) {
        $type = $type ?: self::$defaultType;
        $iblock = $iblock ?: self::$defaultIblock;

        $_SESSION[$type][$iblock]["ITEMS"] = array();
    }

    public static function getCompareCount($type = false, $iblock = false) {
        $type = $type ?: self::$defaultType;
        $iblock = $iblock ?: self::$defaultIblock;

        return count($_SESSION[$type][$iblock]["ITEMS"]);
    }
}