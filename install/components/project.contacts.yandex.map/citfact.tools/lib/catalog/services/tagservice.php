<?
/*
 * This file is part of the Studio Fact package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Citfact\Tools\Catalog\Services;

use \Bitrix\Main\Application;
use \Bitrix\Main\Context;

class TagService {

    public static function getRequestPage() {
        $curTag = self::getCurrentTag();
        return $curTag["PROPERTY_URL_VALUE"] ?: false;
    }

    public static function getCurrentTag() {
        $request =  Context::getCurrent()->getRequest();
        $uriParts = explode('?', $request->getRequestUri(), 2);

        return array_shift(self::getTags(array("PROPERTY_URL_SEO" => $uriParts[0])));
    }

    public static function getTags($filter = array()) {
        $arTags = array();
        $tagsFilter = array_merge(array(
            "IBLOCK_ID" => IBLOCK_TAGS
        ), $filter);

        $tagsSelect = array(
            "ID",
            "NAME",
            "PROPERTY_URL",
            "PROPERTY_URL_SEO",
            "PROPERTY_TITLE",
            "PROPERTY_TEXT",
            "PROPERTY_KEYWORDS",
            "PROPERTY_DESCRIPTION"
        );

        $tagsSort = array("SORT" => "asc");
        $dbTags = \CIBlockElement::getList($tagsSort, $tagsFilter, false, false, $tagsSelect);

        while($arTag = $dbTags->fetch()) {
            $arTags[$arTag["PROPERTY_URL_VALUE"]] = $arTag;
        }

        return $arTags;
    }
}
