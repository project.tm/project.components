<?

/*
 * This file is part of the Studio Fact package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Citfact\Tools\Catalog;

use \Bitrix\Main\Loader;
use Bitrix\Highloadblock\HighloadBlockTable as HLTable;


class Brand {

    protected static $hlBrands = 2;

    public static function getList($ids) {
        \CModule::IncludeModule('highloadblock');
        $elements = array();

        $entityDataClass = self::getEntityDataClass(self::$hlBrands);
        $rsData = $entityDataClass::getList(array(
            'filter' => array("UF_XML_ID" => $ids),
            'select' => array('*')
        ));
        while($element = $rsData->fetch()){
            $elements[$element["UF_XML_ID"]] = $element;
        }

        return $elements;
    }

    protected static function getEntityDataClass($blockId) {
        if (empty($blockId) || $blockId < 1) {
            return false;
        }
        $hlBlock = HLTable::getById($blockId)->fetch();
        $entity = HLTable::compileEntity($hlBlock);
        $entityDataClass = $entity->getDataClass();
        return $entityDataClass;
    }
}
