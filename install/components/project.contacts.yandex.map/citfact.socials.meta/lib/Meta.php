<?php

/*
 * This file is part of the Studio Fact package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Citfact;

use \Bitrix\Main\Loader;
use \Bitrix\Main\Config\Option;

class Meta
{

    /**
     * Show view content in header
     * */

    public static function showMetadata()
    {
        global $APPLICATION;
        $APPLICATION->ShowViewContent('metaData');
    }

    /**
     * Add view content with meta on epilog after
     * */

    public static function setMetaData() {

        global $APPLICATION;

        $arSite = \CSite::getById(SITE_ID)->fetch();
        $siteName = $arSite["NAME"];

        $defVars = array(
            "title" => array(
                "names" => array("og:title", "title"),
                "default" => $APPLICATION->getTitle(false)
            ),
            "site_name" => array(
                "names" => array("og:site_name"),
                "default" => $siteName
            ),
            "url" => array(
                "names" => array("og:url"),
                "type" => "url",
                "default" => self::normalizeUrl($APPLICATION->getCurPage())
            ),
            "locale" => array(
                "names" => array("og:locale"),
                "default" => LANGUAGE_ID == "ru" ? "ru_RU" : "en_US"
            ),
            "description" => array(
                "names" => array("description", "og:description"),
                "default" => $APPLICATION->getPageProperty("description")
            ),
            "type" => array(
                "names" => array("og:type"),
                "default" => "website"
            ),
            "fb_app_id" => array(
                "names" => array("fb:app_id"),
                "default" => Option::get("socialservices", "facebook_appid")
            ),
        );

        $APPLICATION->AddViewContent('metaData', str_replace("\n\r", "", self::getOgParams($defVars)));
    }

    /**
     * Replace default Open Graph params with established
     * @param array $params Default og values
     * @return array Replaced values
     * */

    public static function getOgParams($params) {

        $resultMeta = "";

        global $APPLICATION;

        foreach($params as $key => $param) {
            $paramValue = $APPLICATION->getPageProperty("og:".$key);
            $resultMeta .= self::getTemplateParam(
                $param["names"],
                strlen($paramValue) ? $paramValue : $param["default"]
            );
        }

        if($image = $APPLICATION->getPageProperty("og:image")) {
            $resultMeta .= self::getTemplateImage($image);
        }

        return $resultMeta;
    }


    /**
     * Get absolute link value
     * @param string $url Relative link values
     * @return string Absolute link value
     * */


    public static function normalizeUrl($url)
    {
        $arUrl = parse_url($url);

        $normalizedUrl = $arUrl["scheme"] ? $arUrl["scheme"] : "http";
        $normalizedUrl .= "://";
        $normalizedUrl .= $arUrl["host"] ?: $_SERVER["SERVER_NAME"] ?: $_SERVER["HTTP_HOST"];
        $normalizedUrl .= $arUrl["path"];

        return $normalizedUrl;
    }

    /**
     * Template for default meta value
     * @param array $names tag names
     * @param string $value tag value
     * @return string Meta string
     * */

    private static function getTemplateParam($names, $value) {
        $result = "";
        foreach($names as $name) {
            $result .= "<meta name='$name' content='".htmlspecialchars($value, ENT_QUOTES)."' />";
        }
        return $result;
    }


    /**
     * Template for image value
     * @param string $value tag value
     * @return string Meta string
     * */


    private static function getTemplateImage($value) {
        return "<meta name='og:image' content='".self::normalizeUrl($value)."' />".
               "<link rel='image_src' href='".self::normalizeUrl($value)."' />";
    } 

}








