<?php

/*
 * This file is part of the Studio Fact package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Bitrix\Main\Loader;

Loader::registerAutoLoadClasses('citfact.socials.meta', array(
    'Citfact\Meta' => 'lib/Meta.php',

));
