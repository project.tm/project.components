<?
global $DB, $DBType;
use Bitrix\Main\Loader;

Loader::includeModule('iblock');
Loader::includeModule('highloadblock');
Loader::includeModule('citfact.form');
Loader::includeModule('sale');

Loader::registerAutoLoadClasses('citfact.orderclick', array(
    'Citfact\OrderClick\Order' => 'lib/Order.php',
    'Citfact\OrderClick\Builder\OrderBuilder' => 'lib/Builder/OrderBuilder.php',
    'Citfact\OrderClick\Storage\OrderStorage' => 'lib/Storage/OrderStorage.php',
    'Citfact\OrderClick\OrderUser' => 'lib/OrderUser.php',
    'Citfact\OrderClick\Form' => 'lib/Form.php',
));

CModule::AddAutoloadClasses(
    "webdebug.options",
    array(
        "CWebdebugOptions" => "classes/{$DBType}/CWebdebugOptions.php"
    )
);
?>