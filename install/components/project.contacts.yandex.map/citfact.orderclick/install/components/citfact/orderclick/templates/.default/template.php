<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$path = sprintf('%s%s', getenv('DOCUMENT_ROOT'), $templateFolder);
if (file_exists($path . '/user_type.php')) {
    include $path . '/user_type.php';
}

if (!$arResult['IS_AJAX']) {
    CJSCore::Init(array('date'));
}
?>
    <?if (!$arResult['IS_AJAX']) {?>
    <form action="<?= POST_FORM_ACTION_URI ?>"
          method="post"
          enctype="multipart/form-data"
          data-ng-controller="FormController as form"
          data-ng-init="
            form.options.id = 'form-container-<?=$arResult['COMPONENT_ID']?>';
            form.options.uri = '<?= POST_FORM_ACTION_URI ?>';
            form.options.modal = 'orderClick';

            form.sendData.CSRF = '<?= $arResult['CSRF'] ?>';
            form.sendData.ACTIVE_FROM = '<?=date("d.m.Y H:i:s")?>';
            form.sendData.COMPONENT_ID = '<?= $arResult['COMPONENT_ID'] ?>';
            form.sendData.PRODUCT_ID = '<?= $arParams['PRODUCT_ID'] ?>';
          "
          data-ng-submit="form.sendForm($event)"
          id="form-container-<?=$arResult['COMPONENT_ID']?>">
    <?}?>

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Купить в 1 клик</h4>
            <?/*<div class="item-name">Просторная квартира в самом центре Москвы</div>*/?>
         </div>
        <div class="modal-body">

            <? if ($arResult['SUCCESS'] === true): ?>
                <div class="alert alert-success"><?= GetMessage('SUCCESS_MESSAGE') ?></div>
                <?foreach($arResult["VIEW"] as &$view) {
                    $view["VALUE_LIST"] = array();
                }?>
            <? endif; ?>

            <? if (sizeof($arResult['ERRORS']['LIST']) > 0): ?>
                <div class="alert alert-danger">
                    <? foreach ($arResult['ERRORS']['LIST'] as $type => $value): ?>

                        <? if ($type == 'CAPTCHA' || $type == 'CSRF') { ?>
                            <div><?= GetMessage($value) ?></div>
                        <? } elseif($type == "NAME") { ?>
                            <div><?= GetMessage("NAME_ERROR") ?></div>
                        <? } else { ?>
                            <div><?= $value ?></div>
                        <? } ?>
                    <? endforeach; ?>
                </div>
            <? endif; ?>



            <?
            $nameValue = array_shift($arResult["VIEW"]["NAME"]["VALUE_LIST"]);
            ?>
            <div class="input-line">
                <div class="title">Ваше имя:<span class="red-star">*</span></div>
                <input type="text"
                       value="<?= $nameValue ?>"
                       name="<?= $arResult["VIEW"]["NAME"]["NAME"] ?>"
                       data-ng-model="form.sendData.<?= $arResult["VIEW"]["NAME"]["NAME"] ?>"
                       placeholder="Укажите ваше имя">
            </div>

            <?
            $telephoneValue = array_shift($arResult["VIEW"]["PHONE"]["VALUE_LIST"]);
            ?>
            <div class="input-line">
                <div class="title">Телефон для связи:<span class="red-star">*</span></div>
                <input type="text"
                       value="<?= $telephoneValue ?>"
                       name="<?= $arResult["VIEW"]["PHONE"]["NAME"] ?>"
                       data-ng-model="form.sendData.<?= $arResult["VIEW"]["PHONE"]["NAME"] ?>"
                       placeholder="Укажите ваш телефон">
            </div>




        </div>
        <div class="modal-footer">
            <div class="btn-box align-center">
                <div class="hidden">
                    <input type="text"
                           name="CSRF"
                           value="<?= $arResult['CSRF'] ?>"
                           data-ng-model="form.sendData.CSRF"

                        />
                    <input type="text"
                           name="PRODUCT_ID"
                           value="<?= $arParams['PRODUCT_ID'] ?>"
                           data-ng-model="form.sendData.PRODUCT_ID"

                        />
                    <input type="text"
                           name="ACTIVE_FROM"
                           value="<?=date("d.m.Y H:i:s")?>"
                           data-ng-model="form.sendData.ACTIVE_FROM"

                        />
                    <input type="text"
                           name="COMPONENT_ID"
                           value="<?= $arResult['COMPONENT_ID'] ?>"
                           data-ng-model="form.sendData.COMPONENT_ID"

                        />
                </div>

                <input type="submit"
                       class="btn btn-yellow btn-border"
                       data-ng-click="form.sendForm($event)"
                       data-send-text="<?= GetMessage('FILED_SEND_SUBMIT') ?>"
                       data-default-text="<?= GetMessage('FILED_SUBMIT') ?>"
                       value="<?= GetMessage('FILED_SUBMIT') ?>"/>
            </div>
        </div>
    <?if (!$arResult['IS_AJAX']) {?>
    </form>
    <?}?>
