<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Config;
use Citfact\Form;
use Citfact\OrderClick;
use Citfact\Form\Type\ParameterDictionary;

Loader::includeModule('citfact.form');
Loader::includeModule('citfact.orderclick');

$app = Application::getInstance();
$params = new ParameterDictionary($arParams);
$result = new ParameterDictionary();

if ($params->get('TYPE') == 'IBLOCK') {
    $builder = '\\Citfact\\Form\\Builder\\IBlockBuilder';
    $storage = '\\Citfact\\Form\\Storage\\IBlockStorage';
    $validator = '\\Citfact\\Form\\Validator\\IBlockValidator';
} elseif ($params->get('TYPE') == 'HLBLOCK') {
    $builder = '\\Citfact\\Form\\Builder\\UserFieldBuilder';
    $storage = '\\Citfact\\Form\\Storage\\HighLoadBlockStorage';
    $validator = '\\Citfact\\Form\\Validator\\UserFieldValidator';
} else {
    die("Unknown storage");
}
$orderBuilder = 'Citfact\\OrderClick\\Builder\\OrderBuilder';
$orderStorage = 'Citfact\\OrderClick\\Storage\\OrderStorage';

$form = new \Citfact\OrderClick\Form($params);

$form->register('builder', $builder);
$form->register('validator', $validator);
$form->register('storage', $storage);

$form->register('orderBuilder', $orderBuilder);
$form->register('orderStorage', $orderStorage);

$form->buildForm();
$form->handleRequest($app->getContext()->getRequest());

if ($form->isValid()) {

    $form->save();
}

$result->set('BUILDER', $form->getBuilder()->getBuilderData());
$result->set('VIEW', $form->getViewData());
$result->set('SUCCESS', $form->isValid());
$result->set('ERRORS', $form->getErrors(false));
$result->set('REQUEST', $form->getRequestData());
$result->set('CSRF', $form->getCsrfToken());
$result->set('CAPTCHA', $form->getCaptchaToken());
$result->set('COMPONENT_ID', $form->getIdentifierToken());
$result->set('IS_POST', $form->getRequest()->isPost());
$result->set('IS_AJAX', (getenv('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'));






if ($result->get('IS_AJAX') && $form->isSubmitted()) {
    $GLOBALS['APPLICATION']->restartBuffer();
    header('Content-Type: application/json');

    ob_start();
    $arResult = $result->toArray();
    $this->includeComponentTemplate();
    $bufferTemplate = ob_get_contents();
    ob_clean();

    $response = array(
        'success' => $result->get('SUCCESS'),
        'errors' => $result->get('ERRORS'),
        'captcha' => $result->get('CAPTCHA'),
        'html' => $bufferTemplate,
    );

    exit(json_encode($response));
}


/*$arResult = OrderClick::UserFieldsInterface(
    $_REQUEST,
    $arParams["OBJECT_USER_FILDS"],
    $arParams["SELECT_PLACEHOLDER"],
    $arParams["CLASS_INPUT_TYPE_TEXT"]
);*/
/*
if (!$arResult["ERROR"]) {


    if ($arParams["BUY_USER"]) {
        $buyer = $arParams["BUY_USER"];
    } else {
        $buyer = OrderClick::UserCreate(
            $arResult["INTERFACE"][$arParams["NAME_USER_FILDS"]]["VALUE"], // NAME USER
            $arResult["INTERFACE"][$arParams["PHONE_USER_FILDS"]]["VALUE"], // PHONE USER
            $arParams["DOMAIN_USER_MAIL"], // DOMAIN EMAIL
            $arResult["INTERFACE"][$arParams["LOGIN_USER_FILDS"]]["VALUE"], // LOGIN USER
            $arParams["USER_GROUP"] // GROUP USER
        );
    }

    $currency = CCurrency::GetBaseCurrency();

    $arResult["ORDER_ID"] = OrderClick::AddProduct(
        $arResult["ELEMENT"], // Product ID
        $arResult["QUANTITY"], //QUANTITY
        array(), //arRewriteFields array()*
        false,
        $buyer, // BUYER
        $currency, //CURRENCY
        $arParams["PERSON_TYPE"], //PERSON TYPE
        $arParams["CLEAR_CART"]
    );
}
*/
/*$ajaxId = new CComponentAjax($this->getName(), $this->getTemplateName(), $arParams, null);
$arResult["AJAX_ID"] = $ajaxId->componentID;*/

$arResult = $result->toArray();
$this->includeComponentTemplate();

?>
