<?php

/*
 * This file is part of the Studio Fact package.
 *
 * (c) Kulichkin Denis (onEXHovia) <onexhovia@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Citfact\OrderClick\Builder;

use Citfact\Form\FormBuilderInterface;
use Citfact\Form\Type\ParameterDictionary;
use Citfact\OrderClick\User;

class OrderBuilder implements FormBuilderInterface
{
    /**
     * @var array
     */
    protected $highLoadBlockFields;

    /**
     * @inheritdoc
     */
    public function create(ParameterDictionary $parameters)
    {

        $db_props = \CSaleOrderProps::GetList(
            array("SORT" => "ASC"),
            array(
                "PERSON_TYPE_ID" => $parameters->get('PERSON_TYPE')
            ),
            false,false,array()
        );

        $propertyList = array();

        while($property = $db_props->Fetch()) {
            $propertyList[$property['CODE']] = $property;
        }

        return array(
            'DATA' => $parameters,
            'FIELDS' => $propertyList,
        );
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function getView()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttach()
    {
        return false;
    }
}