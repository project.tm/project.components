<?
/*
 * This file is part of the Studio Fact package.
 *
 * (c) Kulichkin Denis (onEXHovia) <onexhovia@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Citfact\OrderClick;

use Bitrix\Main\Request;
use Bitrix\Main\Application;
use Citfact\Form\Extension;
use Citfact\Form\Exception\ValidateException;
use Citfact\Form\Type\ParameterDictionary;

class OrderUser
{
    public $name;

    public $phone;

    public function __construct(ParameterDictionary $params)
    {
        $app = Application::getInstance();
        $request = $app->getContext()->getRequest();

        $this->params = $params;

        $this->result = $request->getPostList()->toArray();
        $formName = $this->pregArrayKeyExists("/FORM_/", $this->result);
        $this->result = $this->result[$formName];

        $userProps = $params->get("FORM_TO_USER_PROPS");
        $stringPhone = preg_replace('~\D~','',$this->result[$userProps["PHONE"]]);
        $this->phone = $stringPhone;

        if (\CUser::IsAuthorized()) {
            global $USER;
            $this->params->set("id", $USER->getID());
            $this->name = $USER->GetFullName();
        } else {

            if($params->get("REGISTER_TYPE") == "EMAIL") {
                $login = $this->result[$userProps["EMAIL"]];
                $email = $login;
            } else {
                $login = $this->result[$userProps["PHONE"]];
                $email = $stringPhone."@".$this->params->get("DOMAIN_USER_MAIL");
            }

            $usersCheck = \CUser::GetByLogin($login);
            if ($arUser = $usersCheck->Fetch()) {
                $this->params->set("id", (int)$arUser["ID"]);
            } else {
                $name = $this->result[$userProps["NAME"]] ? $this->result[$userProps["NAME"]] : $login;
                $password = self::GeneratePassword(10);

                $user = new \CUser;
                $arFields = Array(
                    "NAME" => $name,
                    "EMAIL" => $email,
                    "LOGIN" => $login,
                    "ACTIVE" => "N",
                    "GROUP_ID" => $this->params->get("USER_GROUP"),
                    "PASSWORD" => $password,
                    "CONFIRM_PASSWORD" => $password,
                    "PERSONAL_PHONE" => $stringPhone
                );
                $this->params->set("id", $user->Add($arFields));
                $this->name = $name;
            }
        }
    }

    protected function pregArrayKeyExists($pattern, $array) {
        $keys = array_keys($array);
        return array_shift(preg_grep($pattern, $keys));
    }

    public function getId() {
        return $this->params->get("id");
    }

    private function GeneratePassword($number)
    {
        $arr = array('a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'r', 's',
            't', 'u', 'v', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F',
            'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'R', 'S',
            'T', 'U', 'V', 'X', 'Y', 'Z',
            '1', '2', '3', '4', '5', '6',
            '7', '8', '9', '0');
        $pass = "";
        for ($i = 0; $i < $number; $i++) {
            $index = rand(0, count($arr) - 1);
            $pass .= $arr[$index];
        }
        return $pass;
    }
}

?>
