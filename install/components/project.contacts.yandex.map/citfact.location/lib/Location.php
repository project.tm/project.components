<?php

/*
 * This file is part of the Studio Fact package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */



namespace Citfact\Location;

use \Bitrix\Main\Application;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Config\Option;
class Location extends \CIblockElement {

    const SESSION_VAR = "CITY_ID";
    const CHANGE_VAR = "changeCity";
	//const IBLOCK_ID = 9;

    public function __construct() {
        if(!(\CModule::IncludeModule("iblock"))) {
        }
        $this->id = $_SESSION[SITE_ID][self::SESSION_VAR];
        $this->prepareCity();
    }

    public function getRealAddress(){
        $ip = false;
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ips = explode (', ', $_SERVER['HTTP_X_FORWARDED_FOR']);
            for ($i = 0; $i < count($ips); $i++) {
                if (!preg_match("/^(10|172\\.16|192\\.168)\\./", $ips[$i])) {
                    $ip = $ips[$i];
                    break;
                }
            }
        }
        return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
    }
    private function setDefaultCity() {
        $this->geoipData = $this->getGeoIpCity();

        if($this->geoipData["city"]) {
           $geoFilter = array("NAME" => $this->geoipData["city"]);
            if($this->trySetCityByFilter($geoFilter))
                return;
        }
        
        $filter = array("PROPERTY_DEFAULT_VALUE" => "Да");
        $this->trySetCityByFilter($filter);
    }

    public function getGeoIpCity() {
        $currentIp = $this->getRealAddress();
        return ALX_GeoIP::GetGeoData($currentIp);
    }

    public function prepareCity() {
        \CModule::IncludeModule("iblock");
        if($this->id) {
            if(!$this->trySetCityById($this->id)) {
                $this->setDefaultCity();
            };
        } else {
            $this->setDefaultCity();
        }
    }
    public function trySetCityById($id) {
        $filter = array("ID" => $id);
        $cityArr = array_shift(self::executeCities($filter));
        if($cityArr) {
            $this->setCityParams($cityArr);
            return true;
        } else {
            return false;
        }
    }
    public function trySetCityByFilter($filter) {
        $cityArr = array_shift(self::executeCities($filter));
        if($cityArr) {
            $this->setCityParams($cityArr);
            return true;
        } else {
            return false;
        }
    }
    private static function executeCities($filter = array()) {
		$filter["IBLOCK_CODE"] = 'CITY';
		$filter["SITE_ID"] = SITE_ID;
        $obCache = Application::getInstance()->getCache();
        if ($obCache->initCache(999, "cache-city-".serialize($filter))) {
            $cacheVars =  $obCache->getVars();
        } else if ($obCache->startDataCache()) {
            $dbCity = self::getList(array(), $filter, false, false, array());
            while($arCity = $dbCity->getNextElement()) {
                $fields = $arCity->getFields();
                $props = $arCity->getProperties();
                $cacheVars[$fields["ID"]]["props"] = $props;
                $cacheVars[$fields["ID"]]["fields"] = $fields;
            }
            if ($cacheVars) {
                $obCache->endDataCache($cacheVars);
            } else {
                $obCache->abortDataCache();
                return false;
            }
        }
        return $cacheVars;
    }

    private function setCityParams($vars) {
        if($vars["fields"]) {
            $this->setObjFields($vars["fields"]);
        }
        if($vars["props"]) {
            $this->setObjProperties($vars["props"]);
        }
        if($cityId = $this->getObjField("ID")) {
            $_SESSION[SITE_ID][self::SESSION_VAR] = $cityId;
        }
    }
    public static function getCities($filter = array()) {
        return self::executeCities($filter);
    }
    public function setObjFields($fields) {
        $this->fields = $fields;
    }
    public function setObjProperties($properties) {
        $this->properties = $properties;
    }
    public function getObjField($fieldName) {
        return $this->fields[$fieldName] ? $this->fields[$fieldName] : false;
    }
    public function getObjFields() {
        return $this->fields;
    }
    public function getObjProperty($propertyName) {
        return $this->properties[$propertyName]["VALUE"] ? $this->properties[$propertyName]["VALUE"] : false;
    }
    public function getObjProperties() {
        return $this->properties;
    }


}