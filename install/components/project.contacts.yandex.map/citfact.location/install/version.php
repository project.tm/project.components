<?php

/*
 * This file is part of the Studio Fact package.
 *
 * (c) Legenko Roman (uNScope) <Roma2091@mail.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

$arModuleVersion = array(
    'VERSION' => '0.6.1',
    'VERSION_DATE' => '2014-10-03 12:00:00',
);
