<?php

/*
 * This file is part of the Studio Fact package.
 *
 * (c) Legenko Roman (uNScope) <Roma2091@mail.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

$MESS['LOCFORM_MODULE_NAME'] = 'Геозависимый контент';
$MESS['LOCFORM_MODULE_DESCRIPTION'] = 'Модуль работы с геозависимым контентом';
$MESS['LOCPARTNER_NAME'] = 'ООО «Центр интернет технологий «ФАКТ»';
$MESS['LOCPARTNER_URI'] = 'http://www.studiofact.ru/';
