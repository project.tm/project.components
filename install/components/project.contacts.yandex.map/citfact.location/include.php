<?php

/*
 * This file is part of the Studio Fact package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Bitrix\Main\Loader;

Loader::includeModule('iblock');

Loader::registerAutoLoadClasses('citfact.location', array(
    'Citfact\Location\ALX_GeoIP' => 'lib/ALX_GeoIP.php',
    'Citfact\Location\Location' => 'lib/Location.php',

));
