<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if (!(empty($arResult['ITEMS']))){
    foreach ($arResult['ITEMS'] as $arItem) {
        $arrFilter['ID'][] = $arItem['IBLOCK_SECTION_ID'];
    }
    $arrFilter['IBLOCK_SECTION_ID'] = $arResult['ID'];
    $rsSect = CIBlockSection::GetList(array('sort' => 'asc'), $arrFilter, false, array('NAME', 'ID'));
    while ($arSect = $rsSect->GetNext()) {
        $sectionList[$arSect['ID']] = $arSect;
    }
    foreach ($arResult['ITEMS'] as $arItem) {
        $sectionList[$arItem['IBLOCK_SECTION_ID']]['ITEMS'][] = $arItem;
    }
    $arResult['SECTIONS'] = $sectionList;
    $items = array();

    foreach ($arResult["SECTIONS"] as $key => $arSect) {
        foreach ($arSect["ITEMS"] as $k => $arItem) {
            $items[$arItem["ID"]] = array(
                "name" => htmlspecialchars_decode(addslashes($arItem["NAME"])),
                "id" => $arItem["ID"],
                "coord" => explode(",", $arItem["PROPERTIES"]["COORDS"]["VALUE"]),
                "address" => htmlspecialchars_decode(addslashes($arItem["PROPERTIES"]["ADDRESS"]["VALUE"])),
                "phone" => $arItem["PROPERTIES"]["PHONE"]["VALUE"],
                "phone_class" => $arItem["PROPERTIES"]["PHONE"]["DESCRIPTION"],
                "graph" => $arItem["PROPERTIES"]["GRAPH"]["VALUE"],
                "email" => $arItem["PROPERTIES"]["EMAIL"]["VALUE"],
                "scheme"=>CFile::ResizeImageGet(
                    $arItem["PROPERTIES"]["SCHEME"]["VALUE"],
                    Array("width"=>613, "height"=>563),
                    BX_RESIZE_IMAGE_PROPORTIONAL,
                    true)
            );
            if (empty($items[$arItem["ID"]]['scheme']['src']))
            {$items[$arItem["ID"]]['scheme']['src']='/images/no-photo.jpg';}
            foreach($items[$arItem["ID"]]['phone_class'] as $classKey => $phone_classes) {
                if (empty($phone_classes)) {
                    $items[$arItem["ID"]]['phone_class'][$classKey] = 1;
                }
            }
        }
        foreach ($items as $city) {
            $itemsQuan[$city['id']] = $city;
        }
        $arResult["SECTIONS"][$key]['ITEMS'] = $items;
        unset($items);
    }
    $arResult["ITEMS"] = $itemsQuan;
}

