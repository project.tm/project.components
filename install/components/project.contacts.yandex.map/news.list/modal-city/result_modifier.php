<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if (!(empty($arResult['ITEMS']))){
    foreach ($arResult['ITEMS'] as $arItem) {
        $arrFilter['ID'][] = $arItem['IBLOCK_SECTION_ID'];
    }
    $arrFilter['IBLOCK_SECTION_ID'] = $arResult['ID'];
    $rsSect = CIBlockSection::GetList(array('sort' => 'asc'), $arrFilter, false, array('NAME', 'ID'));
    while ($arSect = $rsSect->GetNext()) {
        $sectionList[$arSect['ID']] = $arSect;
    }
    foreach ($arResult['ITEMS'] as $arItem) {
        $sectionList[$arItem['IBLOCK_SECTION_ID']]['ITEMS'][] = $arItem;
    }
    $arResult['SECTIONS'] = $sectionList;
    $items = array();

    foreach ($arResult["SECTIONS"] as $key => $arSect) {
        foreach ($arSect["ITEMS"] as $k => $arItem) {
            $items[$arItem["ID"]] = array(
                "name" => htmlspecialchars_decode(addslashes($arItem["NAME"])),
                "id" => $arItem["ID"],
                "coord" => explode(",", htmlspecialchars_decode(addslashes($arItem["PROPERTIES"]["COORDS"]["VALUE"]))),
                "address" => htmlspecialchars_decode(addslashes($arItem["PROPERTIES"]["ADDRESS"]["VALUE"])),
            );
        }
        foreach ($items as $city) {
            $itemsQuan[$city['id']] = $city;
        }
        $arResult["SECTIONS"][$key]['ITEMS'] = $items;
        unset($items);
    }
    $arResult["ITEMS"] = $itemsQuan;
}