<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$firstItem = $arResult["ITEMS"][$arParams["LOCATION_ID"]];
?>
<div class="city-map">
    <div class="city-map__left">
        <div class="city-map__label">
            Выберите город
        </div>
        <div class="city-map__main">
            <div class="city-map__input">
                <input data-city-filter="city-filter-modal" type="text" placeholder="Найти город">
            </div>
            <div class="city-map__items" data-pjax-container="city-filter-modal">
                <? if (empty($arResult["ITEMS"])) {
                    ?>
                    <div class="contacts-info__city">Ничего не найдено</div>
                    <?
                } else {
                    foreach ($arResult["SECTIONS"] as $key => $arSection) { ?>
                        <div class="city-map__item">
                            <div class="city-map__item-heading">
                                <?= $arSection['NAME'] ?>
                            </div>
                            <div class="city-map__item-list">
                                <? foreach ($arSection['ITEMS'] as $arItem) { ?>
                                    <div class="city-map__item-link">
                                        <?$frame=$this->createFrame()->begin()?>
                                        <a class="<?=($arParams["LOCATION_ID"]==$arItem['id'])?'active':''?>" data-change-location="<?= $arItem['id'] ?>"
                                           href="javascript:void(0);"><?= $arItem['name'] ?></a>
                                        <?$frame->end()?>
                                    </div>
                                <? } ?>

                            </div>
                        </div>
                    <? }
                } ?>
            </div>
            <div class="city-map__choose">
                <a id="change-city" href="javascript:void(0);" class="btn btn--yellow">Выбрать</a>
            </div>
        </div>
    </div>
    <?$response = array(
        "items" => $arResult["ITEMS"],
        "selected" => $arParams["LOCATION_ID"]
    )?>
    <div class="city-map__right">
        <?$frame=$this->createFrame()->begin()?>
        <div data-modal-map='<?= json_encode($response) ?>' class="city-map__map" id="modal-map">
        </div>
        <?$frame->end()?>
    </div>
</div>