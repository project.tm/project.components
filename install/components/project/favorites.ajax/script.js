(function (window) {
    'use strict';

    if (window.jsProjectFavorites) {
        return;
    }

    window.jsProjectFavorites = function (arParams, togge) {
        let self = this;
        self.config = {
            ajax: arParams.AJAX,
            select: arParams.SELECT,
            infavorites: arParams.INFAVORITES
        };
        self.param = {
            TYPE: arParams.TYPE
        };

        $(document).on('click', self.config.select, function () {
            var el = $(this);
            if (el.data('call')) {
                return;
            }
            el.data('call', true);
            if (jsIsAuth) {
                self.param.ELEMENT_ID = el.data('id');
                self.param.ADD = el.is('.added') ? 0 : 1;
                $.get(self.config.ajax, self.param, function (data) {
                    if (data && data.success) {
                        self.togge(self.item(self.param.ELEMENT_ID));
                    } else {
                        el.notify('Ошибка на сервере, повторите запрос позднее');
                    }
                    el.data('call', false);
                }, 'json').fail(function () {
                    el.data('call', false);
                    el.notify('Ошибка на сервере, повторите запрос позднее');
                });
            } else {
                el.data('call', false);
                el.notify('Для добавления товара в избранное Вам необходимо войти в личный кабинет или зарегистрироваться');
            }
        });

        if (self.config.infavorites) {
            for (let i in self.config.infavorites) {
                let item = self.item(self.config.infavorites[i]);
                if (!item.data('set')) {
                    self.togge(item);
                    item.data('set', true);
                }
            }
        }
    };

    window.jsProjectFavorites.prototype.item = function (id) {
        return $(this.config.select + '.element' + id);
    };

    window.jsProjectFavorites.prototype.togge = function (item) {
        $(item).toggleClass('added');
        if ($(item).hasClass('added')) {
            $(item).find('span').text(item.data('del'));
        } else {
            $(item).find('span').text(item.data('add'));
        }
    };

})(window);