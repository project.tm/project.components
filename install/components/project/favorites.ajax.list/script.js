(function (window) {

    if (window.jsProjectFavoritesList) {
        return;
    }

    window.jsProjectFavoritesList = function (arParams) {
        var self = this;
        self.config = {
            ajax: arParams.AJAX,
            select: arParams.SELECT,
            contaner: arParams.CONTANER,
            contanerDelete: arParams.CONTANER_DELETE
        };
        self.param = {
            TEMPLATE_NAME: arParams.TEMPLATE_NAME,
            TYPE: arParams.TYPE,
            PAGEN_1: arParams.PAGEN,
        };
        $(document).on('click', self.config.contaner + ' ' + self.config.contanerDelete, function () {
            self.param.ITEM = $(this).data('item');
            if (!self.param.ITEM) {
                self.param.ITEM = self.param.TYPE;
            }
            self.param.ELEMENT_ID = $(this).data('id');
            $.get(self.config.ajax, self.param, function (data) {
                $(self.config.contaner).html(data.content);
            }, 'json');
        });
    };
})(window);