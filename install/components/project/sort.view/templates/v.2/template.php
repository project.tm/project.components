<div class="catalog__view"> 
    <div class="catalog__view-type">
        <span>Вид списка: </span>
        <a class="catalog__view-tile <?= isset($arResult['arView']['card']['select']) ? 'active' : '' ?>" href="<?= $arResult['arView']['card']['url'] ?>"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 25.945 25.945" xml:space="preserve">
                <g>
                    <path d="M6.333,0.209H0.991C0.443,0.209,0,0.652,0,1.2v5.343c0,0.546,0.443,0.99,0.991,0.99h5.342 c0.547,0,0.99-0.444,0.99-0.99V1.2C7.323,0.652,6.88,0.209,6.333,0.209z"/>
                    <path d="M15.539,0.209h-5.343c-0.546,0-0.99,0.443-0.99,0.991v5.343c0,0.546,0.444,0.99,0.99,0.99h5.343 c0.548,0,0.991-0.444,0.991-0.99V1.2C16.53,0.652,16.087,0.209,15.539,0.209z"/>
                    <path d="M24.955,0.209h-5.342c-0.548,0-0.991,0.443-0.991,0.991v5.343c0,0.546,0.443,0.99,0.991,0.99h5.342 c0.547,0,0.99-0.444,0.99-0.99V1.2C25.945,0.653,25.502,0.209,24.955,0.209z"/>
                    <path d="M6.333,9.206H0.991C0.443,9.207,0,9.649,0,10.196v5.343c0,0.547,0.443,0.99,0.991,0.99h5.342 c0.547,0,0.99-0.443,0.99-0.99v-5.343C7.323,9.649,6.88,9.206,6.333,9.206z"/>
                    <path d="M15.539,9.206h-5.343c-0.546,0-0.99,0.443-0.99,0.99v5.343c0,0.547,0.444,0.99,0.99,0.99h5.343 c0.548,0,0.991-0.443,0.991-0.99v-5.343C16.53,9.649,16.087,9.206,15.539,9.206z"/>
                    <path d="M24.955,9.206h-5.342c-0.548,0-0.991,0.443-0.991,0.99v5.343c0,0.547,0.443,0.99,0.991,0.99h5.342 c0.547,0,0.99-0.443,0.99-0.99v-5.343C25.945,9.649,25.502,9.206,24.955,9.206z"/>
                    <path d="M6.333,18.412H0.991C0.443,18.412,0,18.856,0,19.402v5.343c0,0.547,0.443,0.991,0.991,0.991h5.342 c0.547,0,0.99-0.444,0.99-0.991v-5.343C7.323,18.856,6.88,18.412,6.333,18.412z"/>
                    <path d="M15.539,18.412h-5.343c-0.546,0-0.99,0.444-0.99,0.99v5.343c0,0.547,0.444,0.991,0.99,0.991h5.343 c0.548,0,0.991-0.444,0.991-0.991v-5.343C16.53,18.856,16.087,18.412,15.539,18.412z"/>
                    <path d="M24.955,18.412h-5.342c-0.548,0-0.991,0.444-0.991,0.99v5.343c0,0.547,0.443,0.991,0.991,0.991 h5.342c0.547,0,0.99-0.444,0.99-0.991v-5.343C25.945,18.856,25.502,18.412,24.955,18.412z"/>
                </g>
            </svg>
        </a>
        <a class="catalog__view-list <?= isset($arResult['arView']['line']['select']) ? 'active' : '' ?>" href="<?= $arResult['arView']['line']['url'] ?>">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 286.054 286.054" xml:space="preserve">
                <g>
                    <path d="M62.574,0H8.939C4.005,0,0,4.005,0,8.939v53.635c0,4.943,4.005,8.939,8.939,8.939h53.635 c4.934,0,8.939-3.996,8.939-8.939V8.939C71.513,4.005,67.509,0,62.574,0z M62.574,107.27H8.939c-4.934,0-8.939,3.996-8.939,8.939 v53.635c0,4.943,4.005,8.939,8.939,8.939h53.635c4.934,0,8.939-3.996,8.939-8.939v-53.635 C71.513,111.266,67.509,107.27,62.574,107.27z M62.574,214.54H8.939c-4.934,0-8.939,3.996-8.939,8.94v53.635 c0,4.943,4.005,8.939,8.939,8.939h53.635c4.934,0,8.939-3.996,8.939-8.939V223.48C71.513,218.536,67.509,214.54,62.574,214.54z M277.115,107.27H116.209c-4.943,0-8.939,3.996-8.939,8.939v53.635c0,4.943,3.996,8.939,8.939,8.939h160.905 c4.943,0,8.939-3.996,8.939-8.939v-53.635C286.054,111.266,282.059,107.27,277.115,107.27z M277.115,0H116.209 c-4.943,0-8.939,4.005-8.939,8.939v53.635c0,4.943,3.996,8.939,8.939,8.939h160.905c4.943,0,8.939-3.996,8.939-8.939V8.939 C286.054,4.005,282.059,0,277.115,0z M277.115,214.54H116.209c-4.943,0-8.939,3.996-8.939,8.939v53.635 c0,4.943,3.996,8.939,8.939,8.939h160.905c4.943,0,8.939-3.996,8.939-8.939V223.48C286.054,218.536,282.059,214.54,277.115,214.54z" />
                </g>
            </svg>
        </a>
    </div>
    <div class="catalog__view-sort">
        <label>Сортировать по:</label>
    </div>
    <div class="catalog__view-sort-select">
        <select name="sort" onchange1="window.location.href = $(this).val();" class=" ">
            <? foreach ($arResult['arSort'] as $key => $value) { ?>
                <option value="<?= $value['url'] ?>" data-value="<?= $key ?>" <? if (isset($value['select'])) { ?>selected="selected"<? } ?> ><?= $value['name'] ?></option>
            <? } ?>
        </select>
    </div>
</div>