<?php

CBitrixComponent::includeComponentClass("project:project.param");

class projectVoteList extends projectParam {

    public function executeComponent() {
        self::projectFilterParam('cache', 'iblock', 'filter', 'sort');
        if ($this->projectCache()) {
            if (self::projectLoader('iblock')) {

                $arSort = self::projectGetSort();
                $arFilter = self::projectGetFilter('iblock');
                $arSelect = self::projectGetSelect();

                preExit($arSort, $arFilter, $arSelect);

                $res = CIBlockElement::GetList($arSort, $arFilter, false, false, array_unique($arSelect));
                $this->arResult['SLIDER'] = array();
                while ($arItem = $res->GetNext()) {
                    Iblock\Component\Tools::getFieldImageData(
                            $arItem, array('PREVIEW_PICTURE', 'DETAIL_PICTURE'), Iblock\Component\Tools::IPROPERTY_ENTITY_ELEMENT, 'IPROPERTY_VALUES'
                    );
                    $this->arResult['SLIDER'][] = $arItem;
                }

                $this->includeComponentTemplate();
            }
        }
    }

}
