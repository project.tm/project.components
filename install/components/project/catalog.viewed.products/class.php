<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Page\Asset,
    Bitrix\Iblock;

CBitrixComponent::includeComponentClass("bitrix:catalog.viewed.products");

class projectCatalogViewedProducts extends CCatalogViewedProductsComponent {

    public function executeComponent() {
        $this->arResult['PRODUCT_ID'] = $this->getProductIdsMap();
        if ($this->arResult['PRODUCT_ID']) {
            $this->includeComponentTemplate();
        }
    }

}
