<div class="base-style contact_seller contact_seller_popup" <? if ($arResult['IS_AJAX']) { ?>style="display: block;"<? } ?>>
    <form id="form_support"  class="<?= $arParams['FORM_SELECT'] ?>">
        <input type="hidden" id="form-send" name="form-send" value="<?= $arResult['FORM_SEND'] ?>">
        <?= bitrix_sessid_post() ?>
        <div class="wrap_h">
            <div class="top_text">связаться с продавцом</div>
        </div>
        <? if (!empty($arResult['ERROR'])) { ?>
            <div class="u-error">
                <? ShowMessage(implode('<br>', $arResult['ERROR'])) ?>
            </div>
        <? } ?>
        <div class="text_textarea">сообщение продавцу:</div>
        <textarea name="MESSAGE"><?= htmlspecialcharsBx($arResult['DATA']['MESSAGE'] ?: '') ?></textarea>
        <button class="enter_textarea">отправить</button>
        <a class="close-popup"></a>
        <div class="form_success" <? if (!empty($arResult['IS_SEND'])) { ?>style="display: block;"<? } ?>>
            <div class="msg">
                сообщение отправлено
            </div>
            <div class="other_msg">ответ вы получите в личных сообщених в вашем кабинете</div>
        </div>
    </form>
</div>