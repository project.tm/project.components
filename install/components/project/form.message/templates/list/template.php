<div class="base-style contact_seller contact_seller_popup_<?=$arParams['GAME']['ID'] ?>" data-theme="<?=$arParams['THEME_ID'] ?>" <? if ($arResult['IS_AJAX']) { ?>style="display: block;"<? } ?>>
    <form id="form_support"  class="<?= $arParams['FORM_SELECT'] ?>">
        <input type="hidden" id="form-send" name="form-send" value="<?= $arResult['FORM_SEND'] ?>">
        <?= bitrix_sessid_post() ?>
        <div class="wrap_h">
            <div class="top_text">Отправить сообщение</div>
        </div>
        <? if (!empty($arResult['ERROR'])) { ?>
            <div class="u-error">
                <? ShowMessage(implode('<br>', $arResult['ERROR'])) ?>
            </div>
        <? } ?>
        <div class="text_textarea">сообщение:</div>
        <textarea name="MESSAGE"><?= htmlspecialcharsBx($arResult['DATA']['MESSAGE'] ?: '') ?></textarea>
        <button class="enter_textarea">отправить</button>
        <a class="close-popup"></a>
        <? if (!empty($arResult['IS_SEND'])) { ?>
            <script>
                portalForumMessageList[<?=$arParams['THEME_ID'] ?>].update(function() {
                    $('.shadow-popup').hide();
                    $('.contact_seller_popup_<?=$arParams['GAME']['ID'] ?>').hide();
                });
            </script>
            <div class="form_success" style="display: block;">
                <div class="msg">
                    сообщение отправлено
                </div>
                <div class="other_msg">ответ вы получите в личных сообщених в вашем кабинете</div>
            </div>
        <? } ?>
    </form>
</div>