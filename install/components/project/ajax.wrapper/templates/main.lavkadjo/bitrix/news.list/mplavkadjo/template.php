<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if (empty($arParams['WRAPPER']) or ( isset($arParams['WRAPPER']) and $arParams['WRAPPER']['IS_FULL'])) { ?>
    <div class="row slider_jo-games lavka-match-height--js <?= $arParams['WRAPPER']["CONTANER_LIST"] ?: '' ?>">
    <? } ?>
    <? if (empty($arResult["ITEMS"])): ?>
        <div class="personal-list-empty">Предложений не добавлено</div>
    <? else: ?>
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="<?= isset($arParams['IS_EDIT']) ? 'col-lg-3' : 'col-lg-2' ?> col-md-3 col-sm-3 col-xs-6 jo-games jo-games__item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <div class="shadow-hover jo-games__shadow lavka-match-height__item--js">
                    <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="game-img-wrap" style="background-image:url('<?= $arItem['PICTURE'] ?>')"></a>
                    <div class="bot_game-card">
                        <div class="bot_name-game"><?= $arItem['NAME'] ?></div>
                        <? foreach ($arItem['PLATFORM'] as $platform): ?>
                            <span class="console"><?= $platform ?></span>
                        <? endforeach; ?>
                        <div class="lavka-bottom">
                            <div class="row row_price-block">
                                <div>
                                    <? if ($arItem['PRICE']): ?>
                                        <div class="col-md-6 col-sm-6 col-xs-6 price no-pdg"><?= $arItem['PRICE'] ?><span class="rub"> р.</span></div>
                                    <? endif; ?>
                                    <? if ($arItem['EXCHANGE']): ?>
                                        <span class="col-md-6 col-sm-6 col-xs-6 swap"><span>обмен</span></span>
                                    <? endif; ?>
                                </div>
                            </div>
                            <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="more-btn">подробнее</a>
                            <? if (isset($arParams['IS_EDIT'])) { ?>
                                <br><a href="/personal<?= $arItem['DETAIL_PAGE_URL'] ?>" class="more-btn">Редактировать</a>
                            <? } ?>
                        </div>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    <? endif; ?>
    <? if (empty($arResult['WRAPPER']) or ( isset($arResult['WRAPPER']) and $arResult['WRAPPER']['IS_FULL'])) { ?>
        <!--<div class="clear"></div>-->
    </div>
<? } ?>
<script>
<? if ($arResult['NAV_RESULT']->NavPageNomer < $arResult['NAV_RESULT']->NavPageCount) { ?>
    <?= $arParams['WRAPPER']['JS_OBJECT'] ?>.showNext(true);
<? } else { ?>
    <?= $arParams['WRAPPER']['JS_OBJECT'] ?>.showNext(false);
<? } ?>
</script>