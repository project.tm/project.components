function curArticlesPageShowMore(pageCount){
	var _this = $(this);
	_this.data('html',_this.html()).text("Загрузка... ");
	
	$.get(location.href.split("?").shift(),{PAGEN_1:curArticlesPage+1,cGetArticle:1},function(data){
		$(".curArticlesPageContainer").append(data);
		if(++curArticlesPage >= pageCount){
			_this.remove();
		}else{
			_this.removeClass('active').html(_this.data('html'));
		}
	}).fail(function(){
		_this
			.text("Не удалось загрузить страницу, попробуйте позже")
			.delay(1500)
			.queue(function(n){$(this).html(_this.data('html'));n();});
	});
}