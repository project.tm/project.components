<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>
<? if (empty($arParams['WRAPPER']['IS_AJAX'])) { ?>
    <div class="<?= $arParams['WRAPPER']["CONTANER"] ?> callback" id="callback-form">
    <? } ?>
    <? if ($arResult["isFormErrors"] == "Y"): ?><div class="submit_message warning" style="opacity: 1;"><?= str_replace('Не заполнены следующие обязательные поля:', 'Заполните поле:', $arResult["FORM_ERRORS_TEXT"]); ?></div><? endif; ?>
    <? if ($arResult["FORM_RESULT"]) { ?>
        <div class="callback__info success"><?= $arResult["FORM_NOTE"] ?></div>
    <? } else { ?>
        <?= $arResult["FORM_NOTE"] ?>
    <? } ?>
    <? if ($arResult["isFormNote"] != "Y") { ?>
        <?= str_replace('method="POST"', 'method="POST" class="' . $arParams['WRAPPER']["CONTANER_FORM"] . '"', $arResult["FORM_HEADER"]) ?>
        <div class="callback__title"> </div>
        <div class="callback__field">
            <input type="text" id="form_text_11" name="form_text_11" placeholder="Ваше имя" required class="required">
        </div>
        <div class="callback__field" id="phone">
            <input type="text" id="form_text_12" name="form_text_12" placeholder="Введите Ваш номер телефона" required class="required">
        </div>
        <? if ($arResult["isUseCaptcha"] == "Y") { ?>
            <div class="callback__field" id="phone">
                <img src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>" class=""/>
                <input type="hidden" name="captcha_sid" value="<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"/>
                <input type="text" id="captcha_word" name="captcha_word" placeholder="Введите код с картинки" required class="required">
            </div>
        <? } ?>
        <div class="callback__info">В ближайшее время с Вами свяжутся наши менеджеры.</div>
        <input type="hidden" name="web_form_submit" value="<?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>" />
        <input type="hidden" name="web_form_apply" value="<?= GetMessage("FORM_APPLY") ?>" />
        <button class="btn btn__callback <?= $arParams['WRAPPER']["CONTANER_FORM_BUTTON"] ?>" type="submit">Заказать звонок</button>
    <? } ?>
</form>
<? if (empty($arParams['WRAPPER']['IS_AJAX'])) { ?>
    </div>
<? } ?>