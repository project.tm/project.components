<?php

use Bitrix\Main\Loader,
    Bitrix\Iblock;

class projectSlider extends CBitrixComponent {

    public function executeComponent() {
        if (empty($this->arParams["CACHE_TIME"])) {
            $this->arParams["CACHE_TIME"] = 36000000;
        }
        $this->arParams["IBLOCK_TYPE"] = trim($this->arParams["IBLOCK_TYPE"]);
        if (empty($this->arParams["IBLOCK_TYPE"])) {
            $this->arParams["IBLOCK_TYPE"] = "news";
        }
        $this->arParams["IBLOCK_ID"] = (int) $this->arParams["IBLOCK_ID"];
        $this->arParams["PARENT_SECTION"] = (int) $this->arParams["PARENT_SECTION"];
        $this->arParams["PARENT_SECTION_CODE"] = $this->arParams["PARENT_SECTION_CODE"] ?: '';
        $this->arParams["INCLUDE_SUBSECTIONS"] = $this->arParams["INCLUDE_SUBSECTIONS"] != "N";

        $this->arParams["SORT_BY1"] = trim($this->arParams["SORT_BY1"]);
        if (empty($this->arParams["SORT_BY1"])) {
            $this->arParams["SORT_BY1"] = "ACTIVE_FROM";
        }
        if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $this->arParams["SORT_ORDER1"])) {
            $this->arParams["SORT_ORDER1"] = "DESC";
        }
        if (empty($this->arParams["SORT_BY2"])) {
            $this->arParams["SORT_BY2"] = "SORT";
        }
        if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $this->arParams["SORT_ORDER2"])) {
            $this->arParams["SORT_ORDER2"] = "ASC";
        }

        if ($this->startResultCache(false, array($this->arParams))) {
            if (Loader::includeModule('iblock')) {

                $arSort = array(
                    $this->arParams["SORT_BY1"] => $this->arParams["SORT_ORDER1"],
                    $this->arParams["SORT_BY2"] => $this->arParams["SORT_ORDER2"],
                );

                $arFilter = Array(
                    "IBLOCK_TYPE" => $this->arParams['IBLOCK_TYPE'],
                    "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
                    "ACTIVE" => "Y",
                );

                $this->arParams["PARENT_SECTION"] = CIBlockFindTools::GetSectionID(
                                $this->arParams["PARENT_SECTION"],
                                $this->arParams["PARENT_SECTION_CODE"],
                                array(
                                    "GLOBAL_ACTIVE" => "Y",
                                    "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
                                )
                );
                if ($this->arParams["PARENT_SECTION"]) {
                    $arFilter['SECTION_ID'] = $this->arParams['PARENT_SECTION'];
                }

                if ($this->arParams["INCLUDE_SUBSECTIONS"]) {
                    $arFilter['INCLUDE_SUBSECTIONS'] = 'Y';
                }
                
                if (!empty($this->arParams["FILTER_NAME"]) and ! preg_match("/^[a-z_][a-z0-9_]*$/i", $this->arParams["FILTER_NAME"])) {
                    $arrFilter = $GLOBALS[$this->arParams["FILTER_NAME"]];
                    if ($arrFilter) {
                        $arFilter = $arrFilter + $arrFilter;
                    }
                }

                $arSelect = array("ID", "NAME");
                if (!empty($this->arParams['FIELD_CODE'])) {
                    foreach ($this->arParams['FIELD_CODE'] as $value) {
                        if ($value) {
                            $arSelect[] = $value;
                        }
                    }
                }
                if (!empty($this->arParams['PROPERTY_CODE'])) {
                    foreach ($this->arParams['PROPERTY_CODE'] as $value) {
                        if ($value) {
                            $arSelect[] = 'PROPERTY_' . $value;
                        }
                    }
                }

                $res = CIBlockElement::GetList($arSort, $arFilter, false, false, array_unique($arSelect));
                $this->arResult['SLIDER'] = array();
                while ($arItem = $res->GetNext()) {
                    Iblock\Component\Tools::getFieldImageData(
                            $arItem,
                            array('PREVIEW_PICTURE', 'DETAIL_PICTURE'),
                            Iblock\Component\Tools::IPROPERTY_ENTITY_ELEMENT,
                            'IPROPERTY_VALUES'
                    );
                    $this->arResult['SLIDER'][] = $arItem;
                }

                $this->includeComponentTemplate();
            }
        }
    }

}
