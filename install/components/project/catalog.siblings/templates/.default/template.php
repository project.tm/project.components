<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();



$this->setFrameMode(true);

if (!empty($arResult['SIBLINGS']))
{

	?>
	<div id="product-listing" class="product-listing product-listing--new">
		<? if ($arResult['SIBLINGS']['PREV'] != '')
		{
			?>
			<div id="listing-prev">
				<a href="<?= $arResult['SIBLINGS']['PREV']['DETAIL_PAGE_URL'] ?>" target="_self">
					<img src="/local/templates/opticsite/images/prev-product.png" alt=""/><span><?= GetMessage('CT_BCS_CATALOG_PREV') ?></span>
				</a>
			</div>
			<?
		}
		?>
		<? if ($arResult['SIBLINGS']['NEXT'] != '')
		{
			?>
			<div id="listing-next">
				<a href="<?= $arResult['SIBLINGS']['NEXT']['DETAIL_PAGE_URL'] ?>" target="_self">
					<img src="/local/templates/opticsite/images/next-product.png" alt=""/><span><?= GetMessage('CT_BCS_CATALOG_NEXT') ?></span>
				</a>
			</div>
			<?
		}
		?>
	</div>
	<?
}