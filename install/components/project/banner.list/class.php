<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Page\Asset,
    Bitrix\Iblock;

class projectBannerList extends CBitrixComponent {

    private function startWrapperTemplate() {
        $key = sha1($this->GetTemplateName() . serialize($this->arParams) ?: '');
        $this->arResult['CONTANER'] = 'ajax-banner-list-contaner' . $key;
        $this->arResult['CONTANER_LIST'] = 'ajax-banner-list' . $key;

        $jsParams = array(
            'banner' => $this->arResult['BANNER'],
            'count' => (int) $this->arParams['NEWS_COUNT'],
            'contaner' => '.' . $this->arResult['CONTANER'],
            'contanerList' => '.' . $this->arResult['CONTANER_LIST'],
        );
        ?>
        <script>
            jsProjectBannerList(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
        </script>
        <?
    }

    public function executeComponent() {
        if (empty($this->arParams["CACHE_TIME"])) {
            $this->arParams["CACHE_TIME"] = 36000000;
        }
        $this->arParams["IBLOCK_TYPE"] = trim($this->arParams["IBLOCK_TYPE"]);
        if (empty($this->arParams["IBLOCK_TYPE"])) {
            $this->arParams["IBLOCK_TYPE"] = "news";
        }
        $this->arParams["IBLOCK_ID"] = (int) $this->arParams["IBLOCK_ID"];
        $this->arParams["NEWS_COUNT"] = (int) $this->arParams["NEWS_COUNT"] ?: 2;
        $this->arParams["PARENT_SECTION_ID"] = (int) $this->arParams["PARENT_SECTION_ID"];
        $this->arParams["PARENT_SECTION_CODE"] = $this->arParams["PARENT_SECTION_CODE"] ?: '';
        $this->arParams["INCLUDE_SUBSECTIONS"] = $this->arParams["INCLUDE_SUBSECTIONS"] != "N";

        Asset::getInstance()->addJs($this->GetPath() . '/script.js');
        Asset::getInstance()->addCss($this->GetPath() . '/style.css');
        if ($this->startResultCache(false, array($this->arParams))) {
            if (Loader::includeModule('iblock')) {

                $arFilter = Array(
                    "IBLOCK_TYPE" => $this->arParams['IBLOCK_TYPE'],
                    "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
                    "ACTIVE" => "Y",
                );

                $this->arParams["PARENT_SECTION_ID"] = CIBlockFindTools::GetSectionID(
                                $this->arParams["PARENT_SECTION_ID"],
                                $this->arParams["PARENT_SECTION_CODE"],
                                array(
                                    "GLOBAL_ACTIVE" => "Y",
                                    "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
                                )
                );
                if ($this->arParams["PARENT_SECTION_ID"]) {
                    $arFilter['SECTION_ID'] = $this->arParams['PARENT_SECTION_ID'];
                }

                if ($this->arParams["INCLUDE_SUBSECTIONS"]) {
                    $arFilter['INCLUDE_SUBSECTIONS'] = 'Y';
                }

                if (!empty($this->arParams["FILTER_NAME"]) and ! preg_match("/^[a-z_][a-z0-9_]*$/i", $this->arParams["FILTER_NAME"])) {
                    $arrFilter = $GLOBALS[$this->arParams["FILTER_NAME"]];
                    if ($arrFilter) {
                        $arFilter = $arrFilter + $arrFilter;
                    }
                }

                $arSelect = array("ID", "NAME");
                if (!empty($this->arParams['FIELD_CODE'])) {
                    foreach ($this->arParams['FIELD_CODE'] as $value) {
                        if ($value) {
                            $arSelect[] = $value;
                        }
                    }
                }
                if (!empty($this->arParams['PROPERTY_CODE'])) {
                    foreach ($this->arParams['PROPERTY_CODE'] as $value) {
                        if ($value) {
                            $arSelect[] = 'PROPERTY_' . $value;
                        }
                    }
                }

                $res = CIBlockElement::GetList(array(), $arFilter, false, false, array_unique($arSelect));
                $this->arResult['BANNER'] = array();
                while ($arItem = $res->GetNext()) {
                    $this->arResult['BANNER'][] = $arItem['PREVIEW_TEXT'];
                }

                $this->startWrapperTemplate();
                $this->includeComponentTemplate();
            }
        }
    }

}
