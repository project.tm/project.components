(function (window) {
    'use strict';

    if (window.jsProjectBannerList) {
        return;
    }

    window.jsProjectBannerList = function (arParams) {
        $(function () {
            let count = parseInt(arParams.count);
            if (count < 1) {
                count = 1;
            }
            let is = false;
            while (count--) {
                if (arParams.banner.length) {
                    let item = arParams.banner.splice(Math.random() * arParams.banner.length, 1);
                    if (item) {
                        is = true;
                        $(arParams.contanerList).append(item[0]);
                    }
                }
            }
            if (is) {
                $(arParams.contaner).addClass('banner-list-active');
            }
        });
    };

})(window);