<?

use Bitrix\Main\Application;

define('STOP_STATISTICS', true);
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC', 'Y');
define('DisableEventsCheck', true);
define('BX_SECURITY_SHOW_MESSAGE', true);

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

$arResult = array();
ob_start();
$request = Application::getInstance()->getContext()->getRequest();
$arResult['success'] = $APPLICATION->IncludeComponent('project:viewed.ajax', $request->get('TEMPLATE_NAME'), Array(
    'IS_AJAX' => 'Y',
    'TYPE' => $request->get('TYPE'),
    'ELEMENT_ID' => $request->get('ELEMENT_ID'),
        ));
ob_get_clean();
echo json_encode($arResult);
