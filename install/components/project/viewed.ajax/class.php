<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Page\Asset,
    Bitrix\Main\Application,
    Project\Core\Model\FavoritesTable;

class ProjectViewedAjax extends CBitrixComponent {

    private function startWrapperTemplate() {
        Asset::getInstance()->addJs($this->arResult['SCRIPT']);
        $jsParams = array(
            'AJAX' => $this->arResult['AJAX'],
            'TYPE' => $this->arParams['TYPE'],
            'ELEMENT_ID' => $this->arParams['ELEMENT_ID']
        );
        ?>
        <script>
            jsProjectViewedAjax(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
        </script>
        <?
    }

    public function executeComponent() {
        if (Loader::includeModule('iblock')) {
            $this->arParams['TYPE'] = (int) $this->arParams['TYPE'];
            $this->arParams['ELEMENT_ID'] = (int) $this->arParams['ELEMENT_ID'];
            $this->arResult['IS_AJAX'] = isset($this->arParams['IS_AJAX']) and $this->arParams['IS_AJAX'] == 'Y';

            if ($this->arResult['IS_AJAX']) {
                if ($this->arParams['TYPE'] and $this->arParams['ELEMENT_ID']) {
                    $rsProperties = CIBlockElement::GetProperty($this->arParams['TYPE'], $this->arParams['ELEMENT_ID'], "id", "asc", array('CODE' => 'VIEWED'));
                    if ($arProperty = $rsProperties->Fetch()) {
                        CIBlockElement::SetPropertyValues($this->arParams['ELEMENT_ID'], $this->arParams['TYPE'], $arProperty['VALUE']+1, 'VIEWED');
                    }
                    return true;
                }
            }

            $this->arResult['AJAX'] = $this->GetPath() . '/ajax.php';
            $this->arResult['SCRIPT'] = $this->GetPath() . '/script.js';
            $this->arResult['TEMPLATE_NAME'] = $this->GetTemplateName();

            $this->startWrapperTemplate();
        }
    }

}
