<? if ($arResult['STEP'] == 'auth') { ?>
    <script>
        window.location.reload();
    </script>
<? } else { ?>
    <form method="post" action="" class="popup-auth-form popup-auth-form-sms <?= $arParams['FORM_SELECT'] ?>">
        <div class="input_wrap">
            <div class="wrap_only_inp">
                <? if ($arResult['STEP'] == 'phone') { ?>
                    <input type="hidden" name="isPhone" value="1">
                    <div class="bx-authform-label-container">Телефон</div>
                    <input type="text" name="PERSONAL_PHONE" maxlength="50" placeholder="Введите телефон" required class="first-child" <? if (isset($arResult['PHONE'])) { ?>value="<?= addslashes($arResult['PHONE']) ?>"<? } ?>>
                    <? if ($arResult['ERROR']) { ?>
                        <div class="error"><?= $arResult['ERROR'] ?></div>
                    <? } ?>
                    <script>
                        $(function () {
                            window.filterInputmask('[name="PERSONAL_PHONE"]');
                        });
                    </script>
                    <button type="submit" class="registration">ПОЛУЧИТЬ КОД</button>
                <? } else { ?>
                    <div class="bx-authform-label-container">Введите код из sms</div>
                    <input type="hidden" name="isSms" value="1">
                    <input type="hidden" name="PERSONAL_PHONE" value="<?= addslashes($arResult['PHONE']) ?>">
                    <input type="text" name="CODE" maxlength="10" placeholder="Введите код из sms" required class="first-child">
                    <button type="submit" class="registration">ВОЙТИ</button>
                <? } ?>
            </div>
        </div>
    </form>
<? } ?>